function judge_SingleLoop(E, C, B_Dir)

% 函数功能：检查是否含有单向回路
% 输入参数：
%   E -- 分支集合
%   C -- 回路集合
%   B_Direction -- 各回路中分支方向集合
% 步骤：
%   1.先将添加大气分支所在回路去除
%   2.再判断剩余回路的分支方向是否一致
% c = {};
% b = {};
e = E(end, 1); % 分支e为大气分支
[c, b] = RemoveAtmosphericBranches(e, C, B_Dir);
isExist_SingleLoop(c, b);


    function [c, b] = RemoveAtmosphericBranches(e, C, B_Dir)
        % 子函数-1-函数功能：将含有大气分支的回路集合以及分支方向集合至为空矩阵
        % 输入参数：
        %   e -- 大气分支
        %   C -- 回路集合
        %   B_Direction -- 各回路中分支方向集合
        % 输出参数：
        %   c -- 大气分支回路至为空矩阵
        %   b -- 大气分支回路各分支方向至为空矩阵
        for i = 1:length(C)
            childCycle = C{i}; % 遍历回路
            for j = childCycle
                if j == e
                    C{i} = [];
                    B_Dir{i} = [];
                end
            end
        end
        c = C;
        b = B_Dir;
        disp('去除大气分支后分支')
        celldisp(c)
        disp('去除大气分支后回路方向')
        celldisp(b)
    end


    function isExist_SingleLoop(c, b)
        % 子函数-2-函数功能：对去除大气分支之后的回路进行判断
        % 输入参数：
        %   c -- 去除大气分支的回路集合
        %   b -- 去除大气分支的回路方向集合
        for m = 1:length(b)
            childDir = b{m};
            childCycle = c{m};
            n = numel(unique(childDir));
            % 当集合D=[],unique(D)为[],numel(unique(D))为0
            if n > 1
                fprintf('回路%d非单向\n', m);
                disp(childCycle)
                fprintf('各分支方向为\n');
                disp(childDir)
            elseif n == 0
                continue;
            else
                fprintf('回路%d单向\n', m);
                disp(childCycle)
                fprintf('各分支方向为\n');
                disp(childDir)
            end
        end
    end
end