% 测试
clear;
clc;
% 从拓扑数据文件中加载网络
[V, E, from, to, R, F]=loadNetwork('thetaData.txt'); % 普通角联
% [V, E, from, to, R, F]=loadNetwork('P109.txt');
T_Q = 50; % 初始风量
e = [1 6]; % 固定风量分支集合
% e = 10; % e = 1;
% 总结:1.余支均可以
%      2.添加固定风量分支后，若将该分支所在回路去除，去除后回路数量不大于原来的一半，无法解算

% 有些网络没有源汇（例如整个网络就是一个环）需要排除这些情况
if isempty(from) || isempty(to)
    disp('源点或汇点非法')
    return;
end

% 处理多源多汇网络
if length(from)>1 || length(to)>1
    disp('=====================================================')
    disp('该网络是一个多源多汇网络:')
    disp('源点集合:')
    disp(from)
    disp('汇点集合:')
    disp(to)
    disp('==================================================================')
    [V, E, from, to] = dealWithMultiSSNetwork(V, E, from, to);
end

% 将大气分支添加到已有网络中
E(end+1,:) = [max(E(:,1))+1 to from]; % 分支集合:分支编号-始-末
R(end+1) = 0;                         % 风阻值
F(end+1,:) = [0 0 0 0];               % 是否含有风机以及风机参数
% P = [0 -20 -30 -50 -80 -100 -130 -170];
% 将R的值用W保存,在搜索最小生成树时,尽量将大气分支以及含有局部通风设施的分支添加到余支集合当中
W = R;
W(end) = 10000;

% 根据新的分支集合E创建网络表示方法的数据结构
[A, UA, B, Adj_list] = createNetworkStructure(V, E);

% 输出网络信息
printNetworkInfo(V, E, from, to, A, UA, B, Adj_list, 1);

%*****************************************************************************%

SC=findCycle_ByMatrix(A, E, Adj_list);
% % SC = removeAllVirtualEdges(E, SC);  % 删除虚拟分支，同时将分支转换成真实的分支编号
disp('采用节点邻接矩阵计算回路: ')
printCellArrayList(E, SC, 'C',1);

% SC=findCycle_BySearch(E, Adj_list, from);
% SC = removeAllVirtualEdges(E, SC);  % 删除虚拟分支，同时将分支转换成真实的分支编号
% disp('采用dfs搜索回路: ')
% printCellArrayList(E, SC, 'C',1);

isConnectedGraph = checkGraphConnection_ByMatrix(V,E);
disp('采用无向图节点邻接计算图的连通性:')
if isConnectedGraph
    fprintf(sprintf('\t===>图是连通的\n'))
else
    fprintf(sprintf('\t===>图是不连通的\n'))
end

% 采用搜索的方法计算图的连通性

isConnectedGraph = checkGraphConnection_BySearch(E);
disp('采用搜索的方法计算图的连通性:')
if isConnectedGraph
    fprintf(sprintf('\t===>图是连通的\n'))
else
    fprintf(sprintf('\t===>图是不连通的\n'))
end

% 采用矩阵计算方法查找所有通路
OutPaths = findAllPaths_NoCycle_ByMatrix(A, E, from, to);
disp('所有通路矩阵')
celldisp(OutPaths)

% 查找一颗生成树
[ST, C, c] = findMinTree_ByMatrix(E, W, 1);
disp('最小生成树的分支集合')
disp(ST)
disp('各分支权值')
disp(C)
disp('此树总权值')
disp(c)

% 查找余支分支集合TL
TL=TL_Tree(E, ST);
disp('最小生成树对应余支的分支集合')
disp(TL)

% 搜索回路
C = Cycle_BySearch(V, E, TL, 0);
disp('各回路以及各回路的分支编号')
celldisp(C)

% 查找各回路中每一分支的方向(以余支方向为基准)
B_Dir = EveryBranchDirectionInCycle(C, E);
disp('各回路中分支方向')
celldisp(B_Dir)

% 判断是否存在单向回路
judge_SingleLoop(E, C, B_Dir);

disp('=======================================================')
fprintf('自然分风\n')

% 对分支进行风量Q的初始化
InitQ = Init_Q(T_Q, C, B_Dir, ST, TL);
disp('各分支的初始风量')
disp(InitQ)


% 迭代后风量
out_Q = Cycle_Iteration(InitQ, C, B_Dir, F, R);
disp('迭代后风量')
disp(60*out_Q)

% 迭代计算后网络信息
A = printAfterIteration(E, out_Q, R, F);
disp('迭代计算后网络信息')
disp(A)