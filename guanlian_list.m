function Adj_list_1= guanlian_list(V, E)
% 创建网络表示方法的数据结构
% 输入参数：
%   V        --     节点集合
%   E        --     分支集合
% 输出参数:
%   Adj_list_1 --     入边以及出边邻接表

m = length(V); % 节点数
n = size(E,1); % 分支数


% 构造邻接表
Adj_list_1 = cell(m, 1);
for i = 1:m
    for j = 1:n
        v = E(j,2); % 判断始节点
        if v==i
            Adj_list_1{v} = [Adj_list_1{v},j];
        end
        v = E(j,3); % 判断末节点
        if v==i
            Adj_list_1{v} = [Adj_list_1{v}, j];
        end
    end
end

end