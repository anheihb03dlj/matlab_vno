function C = Cycle_BySearch(V,E,TL,hasCycle)
% 在搜索回路之前，将大气分支添加到分支集合中
% 搜索所有回路
% 输入参数：
%   E -- 分支集合
%   V -- 节点集合
%   TL -- 余支集合
%   hasCycle -- 是否含有单向回路（bool类型）
% 输出参数：
%   C -- 回路集合(cell)
% 例如：
%   C1 -- 1 3 4
%   C2 -- 2 4 5
%   ……

C={};
color = zeros(1, size(E, 1)); % 未染色为0
color(TL) = 1;                % 默认余支已经染色为1
Adj_list_1 = guanlian_list(V,E);
for te = TL
    u = E(te, 2); % 该余支的始节点
    v = E(te, 3); % 该余支的末节点
    % fprintf('u=%d,v=%d-->%d',u,v,te);
    % fprintf('分支集合--->%d',te);
    P = dfs_visit(Adj_list_1, E, v, u, color, hasCycle);
    C{end+1}=[te P]; % 将寻找到的te加到P中
end

    function nodes = getNodesOfColoredEdges(E, color)
        nodes = [];
        for ee=1:size(E,1)
            if color(ee)
                 % 只添加着色分支的始节点
                 % 文献2没有说清楚(!!!!)
                nodes = union(nodes, E(ee,2));
            end
        end
    end

    function outEdges = findColoredOutEdges(Adj_list_1, color, u)
        % 函数功能：
        % 根据当前的拓扑结构，查找当前节点u的邻接分支
        % 1）查找有颜色分支
        % 输入参数：
        %   Adj_list_1 -- 邻接表
        %   u -- 要查找出边的节点
        % 输出结果：
        %   outEdges -- 节点u的邻接分支

        outEdges = Adj_list_1{u}; % 提取矩阵A的第u行（等价于获取节点v的出边信息）

        % 查找颜色分支
        outEdges = outEdges(color(outEdges)==1);

    end

    function outEdges = findUnColoredOutEdges_NoCycle(Adj_list_1, color, u)
        % 函数功能：
        % 根据当前的拓扑结构，查找当前节点u的邻接分支
        % 1）查找无颜色分支-- 无单向回路的情况

        outEdges = Adj_list_1{u};
        outEdges = outEdges(color(outEdges)==0);
    end

    function outEdges = findUnColoredOutEdges(Adj_list_1, E, color, u)
        % 函数功能：
        % 根据当前的拓扑结构，查找当前节点u的邻接分支
        % 1）查找无颜色分支-- 有单向回路的情况

        outEdges = [];

        nodes = getNodesOfColoredEdges(E, color);
        % 查找颜色分支
        for ee = Adj_list_1{u}
            if color(ee) || ~isempty(find(nodes==E(ee, 3), 1))
                continue;
            end
            outEdges = [outEdges ee];
        end
    end

    function P = dfs_visit(Adj_list_1, E, u, v, color, hasCycle)
        % 输入参数：
        %   Adj_list_1 -- 节点与分支邻接表
        %   E -- 分支集合
        %   u -- 分支始节点
        %   v -- 分支末节点
        %   color -- 分支颜色
        %   hasCycle -- 是否含有单向回路
        % 输出参数：
        %   P -- 所有节点 u -> v 的路径
        P=[];
        path=[];
        Paths={};
        
        % 此处应该将TL中的每一分支逐条加入到ST中
        while true
            outEdges = [];
            % 修改后的dfs可以同时适用于有无单向回路的情况
            % findUnColoredOutEdges完全可以代替findUnColoredOutEdges_NoCycle
            % 从效率的角度考虑，做成了if/else的形式(!!!!!!!!!!!!)
            if hasCycle
                outEdges = findUnColoredOutEdges(Adj_list_1, E, color, u); % 寻找有单向回路情况的节点u的未着色出边分支
            else
                outEdges = findUnColoredOutEdges_NoCycle(Adj_list_1, color, u);  % 寻找无单向回路情况的节点u的未着色出边分支
            end

            if isempty(outEdges)
                if isempty(path)
                    if isempty(Paths)
                        P=[];
                    else
                        P=Paths{1};
                    end
                    return;
                else
                    if u == E(path(end), 3)  % 若u为末节点
                        u = E(path(end), 2); % 退回到始节点
                    else                     % 否则
                        u = E(path(end), 3); % u为末节点，接着往下搜
                    end
                    path = path(1:end-1);
                end
            else
                for e = outEdges
                    if color(e)
                        continue;
                    end
                    color(e) = 1;
                    path = [path, e];       % 将分支添加到路径中
                    if u == E(e, 2)
                        u = E(e, 3);
                    else
                        u = E(e, 2);
                    end
                    if u == v
                        disp(path);
                        Paths{end+1} = path;
                        if v == E(e, 3)
                            u = E(path(end), 2); % 通路的最后一条分支的始节点作为寻边始节点
                        else
                            u = E(path(end), 3); % 通路的最后一条分支的末节点作为寻边始节点
                        end
                        path = path(1:end-1);
                    end
                    break;
                end
            end
        end
    end
end