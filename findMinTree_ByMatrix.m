function [ST, C, c]=findMinTree_ByMatrix(E, W, flag)

d = [E(:,2) E(:,3) W]'; % 取分支集合的始末节点编号以及权重
% 函数功能：寻找网络中的最小生成树
% 输入参数：
%   E -- 分之集合
%   R -- 权重
% -- 分支始点 -- 分支末点 -- 分支权重 --
%      E(,2)      E(,3)       E(,4)
% 输出参数：
%   ST -- 生成树集合
%   C -- 分支权值集合
%   c -- 该树的权重

if nargin ==1
    n = size(d,2);
    m = sum(sum(d ~= 0))/2;
    b = zeros(3,m);
    k = 1;
    for i = 1:n
        for j = (i+1):n
            if d(i,j) ~= 0
                b(1,k) = i;
                b(2,k) = j;
                b(3,k) = d(i,j);
                k = k+1;
            end
        end
    end
else
    b = d;
end

n = max(max(b(1:2,:)));
m = size(b,2);
[B,i] = sortrows(b',3);
B = B';
c = 0;
T = [];
k = 1;
t = 1:n;
for i = 1:m
    if t(B(1,i)) ~= t(B(2,i))
        T(1:2,k) = B(1:2,i);
        c = c+B(3,i);
        k = k+1;
        tmin = min(t(B(1,i)),t(B(2,i)));
        tmax = max(t(B(1,i)),t(B(2,i)));
        for j = 1:n
            if t(j) == tmax
                t(j) = tmin;
            end
        end
    end
    if k == n
        break;
    end
end


% 得到矩阵T如下：
%   第一行始节点:6 2 2 3 5
%   第二行末节点:1 3 4 5 6

ST = [];
C = [];
for i = 1:length(T(1,:)) % T -- 向量长度
    for j = 1:length(E(:,2)) % E -- 向量长度
        if T(1,i) == E(j,2) && T(2,i) == E(j,3) % T -- 始节点 == E -- 始节点
            ST = [ST E(j,1)]; % 将分支编号保存到ST中
            C = [C W(j)]; % 将权值大小保存到C中
        end
    end
end
% 对ST进行排序




