function [V, E, from, to, R, F] = loadNetwork(filepath)
% 函数功能：从拓扑文件graph_topology.txt中加载网络
% 输出参数：
%   V        --     节点集合
%   E        --     分支集合
%   from     --     源点集合
%   to       --     汇点集合

% 定义分支集合（示例）
% E = [
%     % 格式
%  分支编号 -- 分支始末节点 -- 风阻 --是否含有风机 -- 风机参数(a,b,c)
%     1          1    2       0.1       0            a  b  c
%     2          2    3       0.1       0            a  b  c
%     3          2    4       0.1       0            a  b  c 
%     4          3    4       0.1       0            a  b  c
%     5          3    5       0.1       0            a  b  c
%     6          2    5       0.1       0            a  b  c
%     7          5    6       0.1       0            a  b  c
%     8          6    1        0        1            a  b  c
%     ];
[filename, pathname] = uigetfile('*.txt','请选择一个拓扑数据文件');
InE = load(fullfile(pathname, filename)); % 加载网络

n = size(InE,1); % 分支数

% 构造节点信息映射表
V=[];
for i=1:n
    V = union(V, InE(i, 2:3)); % 提取分支的始末节点，合并重复的节点
end

m = length(V); % 节点数

E = zeros(n, 3); % 记录分支编号
R = zeros(n, 1); % 分支风阻参数
F = zeros(n, 4); % 风机参数
for i=1:n
    E(i,1) = InE(i,1);           % 记录分支的真实编号
    E(i,2) = find(V==InE(i,2));  % 始节点的编号在V中的索引位置
    E(i,3) = find(V==InE(i,3));  % 末节点的编号在V中的索引位置
    R(i) = InE(i,4);             % 该分支的风阻R
    F(i,1) = InE(i,5);           % 该分支是否含有风机
    F(i,2) = InE(i,6);           % =======================================
    F(i,3) = InE(i,7);           % 该分支的风机参数(二次函数仅涉及三个参数)
    F(i,4) = InE(i,8);           % =======================================
end

% 节点度的信息，包括节点的出度outDegree和入度信息inDegree
VertexDegree=cell(m, 1);
for n=1:n
    u = E(n, 2);  % 始节点（节点映射在V中的编号）
    v = E(n, 3);  % 末节点（节点映射在V中的编号）
    if isempty(VertexDegree{u})
        VertexDegree{u} = [VertexDegree{u}; [1 0]]; % 出度初始化为1，初始化为0
    else
        VertexDegree{u}(1) = VertexDegree{u}(1)+1; % 出度+1
    end
    
    if isempty(VertexDegree{v})
        VertexDegree{v} = [VertexDegree{v}; [0 1]]; % 出度初始化为0，初始化为1
    else
        VertexDegree{v}(2) = VertexDegree{v}(2)+1; % 入度+1
    end
end

% 计算源汇点（节点在V中的映射索引，并非真实的节点编号）
from=[];
to=[];
for s=1:length(VertexDegree)
    outDegree = VertexDegree{s}(1);  % 获取节点的出度和入度
    inDegree = VertexDegree{s}(2);
    if outDegree>0 && inDegree==0
        from = [from s];
    elseif outDegree==0 && inDegree>0
        to = [to s];
    end
end

end