function B_Dir = EveryBranchDirectionInCycle(C, E)
% 输入参数：
%   C -- 各回路分支集合
%   E -- 分支集合
% 输出参数：
%   Direction -- 各回路中每一分支的方向

% 初始化一个元胞数组B_Diretion
B_Dir = {};

% 根据节点分支集合E以及该回路C,得到每一分支的始末点

% 规定每一余支的方向为正方向

for i = 1:length(C)
    d = 1; % 每寻找单一回路，余支方向初始化为 -> 1
    Dir{i} = 1;
    for j = 1:length(C{i})-1
        u = E(C{i}(j), 2); % 当前分支的始节点
        v = E(C{i}(j), 3); % 当前分支的末节点
        uu = E(C{i}(j+1), 2); % 相邻下一分支的始节点
        vv = E(C{i}(j+1), 3); % 相邻下一分支的末节点
        % 分支相同：
        %   1. 当前分支始节点为相邻分支的末节点
        %   2. 当前分支末节点为相邻分支的始节点
        % 分支相反：
        %   其他。
        if u == vv || v == uu 
            d = d;
        else
            d = -d;
        end
        % disp(C{i}(j+1))
        % disp(d)
        Dir{i} = [Dir{i} d]; % 保存
    end
end
B_Dir = Dir;