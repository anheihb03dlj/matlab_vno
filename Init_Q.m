function InitQ = Init_Q(T_Q, C, B_Dir, ST, TL)
% (1)大气分支的风量
% (2)余支、树支初始风量(尽可能的大)
% 输入参数：
%   T_Q -- 总风量
%   C -- 回路元组
%   B_Direction -- 各回路元组中各分支的方向
%   ST -- 最小生成树支集合
%   TL -- 余支集合
%   R -- 分支阻力,这里的R为添加大气分支之后,其真实值为R(8,1)=0
% 输出参数：
%   Q -- 各分支的风量
n = length(ST)+length(TL);
InitQ = zeros(n, 1);

% 余支风量先初始化，再根据余支风量求解树支风量的初始值，所以遍历树支集合
InitQ(TL) = T_Q/length(TL); % 余支分支风量初始化

for k = 1:30
    for e = ST
        q = 0;
        for i = 1:length(C) % 对每一回路进行初始化处理
            % c = 0;
            j = find(C{i}==e); % 把C{i}=e的边位置信息赋值给j
            if j
                b = B_Dir{i}(j); % 回路i中的第j个分量方向
                te = C{i}(1); % 回路i中的余支编号
                q = q + b*InitQ(te); % 先根据节点风量平衡得到各分支的风量
                InitQ(e) = q;
            end
        end
        % Q(e) = q;
    end
end
end
