function out_Q = Cycle_Iteration(InitQ, C, B_Dir, F, R)

% 函数功能：根据回路对于每一分支的风量进行迭代计算
% 输入参数：
%   Q -- 初始风量
%   C -- 回路集合
%   B_Direction -- 各回路中各分支的方向
%   F -- 风机特性
%   R -- 各分支风阻
% 输出参数：
%   out_Q -- 各分支风量

% 设置初始迭代参数
n = 20;      % 步骤
out_Q = InitQ;   % 初始值
mu_h = 1;    % 风压误差
mu_q = 1e-4; % 风量误差

for i = 1:n
    % 定义上限
    fprintf('\n第%d次迭代\n',i);
    D_Q = [];
    D_H = [];
    for j = 1:length(C)
        childCycle = C{j};
        childDir = B_Dir{j};
        [delta_q, delta_h] = Delta(out_Q, childCycle, childDir, F, R);
        out_Q = Increase_Q(out_Q, delta_q, childCycle, childDir);
        printCycle(j, childCycle, childDir, delta_q, delta_h);
        % out_Q
        D_Q(end+1) = abs(delta_q);
        D_H(end+1) = abs(delta_h);
    end
    if max(D_H) < mu_h && max(D_H) < mu_q % 同时满足这两个迭代精度，停止迭代
        fprintf('\n实际迭代步骤为i=%d\n', i);
        return;
    end
end

    function [delta_q, delta_h] = Delta(out_Q, childCycle, childDir, F, R)
        % 函数功能：根据回路求解风量校正量以及风压校正量
        % 输入参数：
        %   out_Q -- 各分支初始风量
        %   childCycle -- 各回路
        %   childDir -- 回路中各分支方向
        %   F -- 风机特性
        %   R -- 各分支风阻
        % 输出参数：
        %   delta_q -- 风量校正值
        %   delta_h -- 风压校正值
        
        % 初始化
        h1 = 0;
        h2 = 0;
        h3 = 0;
        h4 = 0;
        for k = 1:length(childCycle)
            e = childCycle(k);
            c = childDir(k);
            h1 = h1 + c*R(e)*abs(out_Q(e))*out_Q(e); % 防止风量相反
            h2 = h2 + 2*abs(c*R(e)*out_Q(e));
            h3 = h3 + f0(e, out_Q, F); % 风机特性曲线0阶导
            h4 = h4 + f1(e, out_Q, F); % 风机特性曲线1阶导
        end
        delta_h = h1 - h3;
        delta_q = -(h1-h3)/(h2-h4);
        fprintf('h1=%f  h2=%f  h3=%f  h4=%f\n', h1, h2, h3, h4)
    end


    function Q_K = Increase_Q(Q_K_1, delta_q, childCycle, childDir)
        % 函数功能：计算相邻两次迭代风量，并返回当前风量值
        % 输入参数：
        %   Q_K_1 -- k-1次迭代的风量Q_K_1
        %   delta_q -- 风量校正值
        %   childCycle -- 各回路
        %   childDir -- 回路中各分支方向
        % 输出参数：
        %   Q_K -- 第k次迭代后的风量Q_K
        Q_K = Q_K_1;
        for m = 1:length(childCycle)
            e = childCycle(m);
            c = childDir(m);
            Q_K(e) = Q_K_1(e) + c*delta_q;
        end
        % Q_K_1
        % Q_K
    end


    function h = f0(e, out_Q, F)
        % 函数功能：求解含有风机的风压值（二次函数）
        % 输入参数：
        %   e -- 回路中各分支编号
        %   out_Q -- 风量值
        %   F -- 风机特性
        % 输出参数：
        %   h -- 风压值
        q = out_Q(e);
        h = 0;
        if F(e, 1)==1
            a0 = F(e, 2); % 常数项
            a1 = F(e, 3); % 一次项
            a2 = F(e, 4); % 二次项
            h =  a0 + a1*q + a2*q^2; % 风压-风量表达式
        end
    end


    function h = f1(e, out_Q, F)
        % 函数功能：对于风压关于风量方程求一次导函数
        q = out_Q(e);
        h = 0;
        if F(e, 1)==1
            a1 = F(e, 3);
            a2 = F(e, 4);
            h =  a1 + 2*a2*q;
        end
    end


    function printCycle(j, childCycle, childDir, delta_q, delta_h)
        % 函数功能：输出当前回路j的分支列表、分支方向、风量调节量以及风压调节量
        fprintf('\n第%d条回路\n',j);
        fprintf('分支列表：');
        for m = 1:length(childCycle)
            e = childCycle(m);
            fprintf('  %d  ', e);
        end
        fprintf('\n');
        fprintf('回路方向：');
        for m = 1:length(childCycle)
            c = childDir(m);
            fprintf('  %d  ', c);
        end
        fprintf('\n回路调节风量：%f',delta_q);
        fprintf('\n回路压差：%f\n',delta_h);
        
    end
        

end
