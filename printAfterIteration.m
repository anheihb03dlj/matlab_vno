function A = printAfterIteration(E, out_Q, R, F)
% 函数功能：将迭代计算后的结果保存起来
% 输入参数：
%   E -- 分支集合
%   out_Q -- 解算后风量
%   R -- 风阻
%   F -- 风机参数
% 输出参数：
%   A -- 迭代计算后结果
%   排列方式
%   分支编号-始节点-末节点-风阻-风量-风压-风机特性
%      e       u     v     R    q    h     F
% 初始化A
m = size(E, 1);
A = zeros(m, 1);
% 将大气分支添加到已有网络中
% E(end+1, :) = [max(E(:,1))+1 to from]; % 分支集合:分支编号-始-末
% R(end+1) = 0;                          % 风阻值
F(end+1,:) = [0 0 0 0];                % 是否含有风机以及风机参数
A(:,1:3) = E(:,1:3);
A(:,4) = R;
A(:,5) = 60.*out_Q;
H = [];
h = 0;
for i = 1:m
    h = R(i)*(60*out_Q(i))^2;
    H(end+1, 1) = h; % 保存成列向量
end
A(:,6) = H;
format long